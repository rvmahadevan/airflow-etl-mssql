from airflow.hooks.dbapi_hook import DbApiHook
import pymssql
import sqlalchemy;

class SqlAlchemyHook(DbApiHook):
    """
    Interact with Microsoft SQL Server through SqlAlchemy.
    """
    conn_name_attr = 'mssql_conn_id'
    default_conn_name = 'mssql_default'
    supports_autocommit = True

    def __init__(self, *args, **kwargs):
        super(SqlAlchemyHook, self).__init__(*args, **kwargs)
        self.schema = kwargs.pop("schema", None)
    
    def get_conn(self):
        """
        Returns a SqlAlchemy connection object
        """
        print("----------------- DATABASE SCHEMA -------------------");
        print(self.schema);
        conn = self.get_connection(self.mssql_conn_id)
        engine = sqlalchemy.create_engine(
            'mssql+pymssql://'+str(conn.login)+':'+str(conn.password)+'@'+str(conn.host)+':'+str(conn.port)+'/'+(self.schema or conn.schema)
            );
        conn = engine.connect();
        return conn;