# from airflow.hooks.dbapi_hook import DbApiHook
from azure.storage.blob import BlockBlobService, PublicAccess, ContentSettings

class AzureCloudProcess:

    def get_azure_blob_conn(self, account, key):
        try:
            # Create the BlockBlockService that is used to call the Blob service for the storage account
            block_blob_service = BlockBlobService(account_name=account, account_key=key)
        except Exception as e:
            print(e)

        return block_blob_service



