# pylint: disable=F0401, invalid-name
import os
import logging
import shutil
import csv
import gzip
import subprocess
from collections import OrderedDict
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException


#from mail.smtp import NotificationMail

class CustomPythonCallable(BaseOperator):
    """
    Custom python function callable class
    """
    def __init__(self,
                 python_callable,
                 *args,
                 **kwargs):
        super(CustomPythonCallable, self).__init__(*args, **kwargs)
        self.python_callable = python_callable

    def execute(self, **kwargs):
        self.python_callable(kwargs['context'])


class CustomPythonFunctionCallable(BaseOperator):
    """
    Custom python function callable class
    """
    def __init__(self,
                 python_callable,
                 *args,
                 **kwargs):
        super(CustomPythonFunctionCallable, self).__init__(*args, **kwargs)
        self.python_callable = python_callable

    def execute(self, *args, **kwargs):
        self.python_callable()


class CustomExtractOperator(BaseOperator):
    """
    Extract files from given location
    """
    template_fields = ("file_path", "dest_path")
    @apply_defaults
    def __init__(self,
                 file_path,
                 dest_path,
                 *args,
                 **kwargs):
        super(CustomExtractOperator, self).__init__(*args, **kwargs)
        self.file_path = file_path
        self.dest_path = dest_path

    def execute(self, context):
        logging.info("Extracting data - %s", self.file_path)
        functions.extract_data(file_location=self.file_path,
                               extracted_file_path=self.dest_path)


class CustomLineCountOperator(BaseOperator):
    """It count the total number of lines in given file
    and stored into xcom object"""
    template_fields = ("file_location",)

    @apply_defaults
    def __init__(self,
                 file_location,
                 xcom_key,
                 file_format='csv',
                 skip_leading_row=0,
                 *args,
                 **kwargs):
        super(CustomLineCountOperator, self).__init__(*args, **kwargs)
        self.file_location = file_location
        self.file_format = file_format
        self.xcom_key = xcom_key
        self.skip_leading_row = skip_leading_row

    def execute(self, context):
        if self.file_format not in ['csv', 'tsv', 'txt']:
            AirflowException('File format not supported')
        try:
            line_count = 0
            logging.info("Counting number of lines for %s", self.file_location)
            with open(self.file_location) as file_object:
                for line in file_object:
                    line_count += 1
            if line_count and self.skip_leading_row:
                line_count -= 1
            context['ti'].xcom_push(key=self.xcom_key, value=line_count)
            logging.info('file count %s', str(line_count))
        except Exception, e:
            AirflowException(e)
            logging.debug(e)


class CustomDeleteOperator(BaseOperator):
    """Delete given folder or file """
    template_fields = ("dir",)

    @apply_defaults
    def __init__(self,
                 dir,
                 isFile=False,
                 *args,
                 **kwargs):
        super(CustomDeleteOperator, self).__init__(*args, **kwargs)
        self.dir = dir
        self.isFile = isFile

    def execute(self, context):
        logging.info("Trying to delete object : %s" % self.dir)
        try:
            if self.isFile:
                os.remove(self.dir)
            else:
                shutil.rmtree(self.dir)
            logging.info("Successfully deleted : %s" % self.dir)
        except Exception, e:
            AirflowException(e)
            logging.debug(e)


class CustomReadVariable(BaseOperator):
    """Delete given folder or file """
    template_fields = ("filepath", )

    @apply_defaults
    def __init__(self,
                 filepath,
                 *args,
                 **kwargs):
        super(CustomReadVariable, self).__init__(*args, **kwargs)
        self.filepath = filepath,
    def execute(self, context):
        logging.info("Trying to read data from : %s" % self.filepath)
        file = self.filepath[0]
        try:

            with open(file, 'r') as file_object:
                for line in file_object:
                    print line
                    st = [ x.replace("\n","") for x in  str(line).split(',')]
                    #st = [x for x in str(line).split(',')]
                    v1, v2, v3, v4, v5, v6, v7, v8, v9  = st
                    bql = 'SELECT * FROM %s' % v1
                    staging_table = '%s.%s' % (v6,v7)
                    dest_table  = v9
                    print (bql)
                    print(staging_table)
                    print(dest_table)

                logging.info("Successfully read : %s" % self.filepath)
        except Exception, e:
            AirflowException(e)
            logging.debug(e)

class CustomReadVariableSQL(BaseOperator):
    """Delete given folder or file """
    template_fields = ("filepath",)

    @apply_defaults
    def __init__(self,
                 smtp_server,
                 filepath,
                 bq_conn_id,
                 delegate_to=None,
                 write_disposition = 'WRITE_TRUNCATE',
                 *args,
                 **kwargs):
        super(CustomReadVariableSQL, self).__init__(*args, **kwargs)
        self.filepath = filepath
        self.bq_conn_id = bq_conn_id
        self.delegate_to = delegate_to
        self.write_disposition = write_disposition
        self.smtp_server = smtp_server

    def execute(self, context):
        logging.info("Trying to read data from : %s" % self.filepath)
        file = self.filepath
        bq_hook = CustomBigQueryHook(bigquery_conn_id=self.bq_conn_id,
                                     delegate_to=self.delegate_to)

        try:
            conn = bq_hook.get_conn()
            cursor = conn.cursor()
            with open(file, 'r') as file_object:
                for line in file_object:
                    st = [ x.replace("\n","") for x in  str(line).split(',')]
                    v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11  = st
                    sender = v10
                    receiver = v11
                    subject = 'BQ View - Data Load completed in %s' % v9
                    text = 'The data in view %s is loaded to %s' % (v1, v9)
                    self.bql = 'SELECT * FROM [%s]' % v1
                    self.destination_dataset_table = '%s.%s' % (v6, v8)
                    cursor.run_query(bql=self.bql,
                                     destination_dataset_table=self.destination_dataset_table, write_disposition = 'WRITE_TRUNCATE')
                    try:
                        pass
                        #NotificationMail(self.smtp_server, receiver, sender, subject, text).send()
                    except:
                        print("No SMTP Server")
                    print self.bql, v1, v9
                logging.info("Successfully read : %s" % self.filepath)
        except Exception, e:
            print e
            AirflowException(e)
            logging.debug(e)


class CustomCreateFileOperator(BaseOperator):
    """Creating file in specified cloud location"""
    template_fields = ("cloud_destination_location", 'filename')
    @apply_defaults
    def __init__(self,
                 conn_id,
                 filename,
                 content,
                 cloud_destination_location,
                 validate=False,
                 *args,
                 **kwargs):
        super(CustomCreateFileOperator, self).__init__(*args, **kwargs)
        self.conn_id = conn_id
        self.filename = filename
        self.content = content
        self.validate = validate
        self.cloud_destination_location = cloud_destination_location

    def get_connection(self, conn_type, conn_id):
        if conn_type == "gs":
            return CustomGcsHook(google_cloud_storage_conn_id=conn_id)
        elif conn_type == "s3":
            return CustomS3Hook(s3_conn_id=conn_id)
        else:
            raise AirflowException("Connection type must be s3 or gs.")

    def execute(self, context):
        try:
            file = open("/tmp/%s" % self.filename, 'w')
            if self.content is not None:
                file.write(self.content)
            file.close()

            for uri in self.cloud_destination_location:
                src_conn_type = functions.obj_scheme(uri)
                bucket_name = functions.obj_bucket(uri)
                key = functions.obj_path(uri)
                src_hook = self.get_connection(conn_type=src_conn_type, conn_id=self.conn_id)
                src_hook.upload(bucket=bucket_name,
                                validate=self.validate,
                                object="%s%s" % (key, self.filename),
                                filename="/tmp/%s" % self.filename)
                logging.info("File uploaded into %s%s" % (uri, self.filename))
        except Exception, e:
            raise AirflowException("Error in genarating file- %s" %e)


class CsvSchemaMerge(BaseOperator):
    """This class compare schema of two files and merge the data and schema to one"""
    template_fields = {'input_file','output_file','schema_file'}

    @apply_defaults
    def __init__(self, input_file, output_file,schema_file,*args,**kwargs):
        super(CsvSchemaMerge, self).__init__(*args, **kwargs)

        self.input_file = input_file
        self.output_file = output_file
        self.schema_file = schema_file
        print self.schema_file



    def execute(self, context):
        # filenames = self.schema_file, self.input_file
        # data = OrderedDict()

        fieldnames = []
        directory = os.path.dirname(self.output_file)
        if not os.path.exists(directory):
            os.makedirs(directory)

        with open(self.schema_file, "rb") as fp:  # python 2
            raw_fieldnames = []
            reader = csv.DictReader(fp)
            raw_fieldnames.extend(reader.fieldnames)
            fieldnames = list(OrderedDict.fromkeys(raw_fieldnames))
            print raw_fieldnames

        with gzip.open(self.input_file, "rb") as fp:  # python 2
            reader = csv.DictReader(fp)
            with gzip.open(self.output_file, "wb") as out_fp:
                writer = csv.writer(out_fp)
                writer.writerow(fieldnames)
                reader = csv.DictReader(fp)
                for row in reader:
                    writer.writerow([row.get(field, '') for field in fieldnames])

                print("Copied Successfully")


class CustomZipSplitOperator(BaseOperator):
    template_fields = ("file_name", 'temp_dir', 'file')

    @apply_defaults
    def __init__(self, temp_dir, file_name, file, report_suite, remove_header=False, *args, **kwargs):
        super(CustomZipSplitOperator, self).__init__(*args, **kwargs)
        self.temp_dir = temp_dir
        self.file_name = file_name
        self.remove_header = remove_header
        self.suite = report_suite
        self.file = file

    def execute(self, context):
        self._file_split(self.temp_dir + '/' + self.file_name)

    def _file_split(self, file):

        # Untar the gzip archive
        if file.split('.')[-1] == 'zip':
            cmd = [
                "unzip",
                file,
                "-d",
                self.temp_dir
            ]
        else:
            cmd = [
                "tar",
                "-zxvf",
                file,
                "-C",
                self.temp_dir
            ]
        logging.info("Running shell command: %s" % cmd)
        code = subprocess.check_call(cmd)
        logging.info("shell command %s returned code %s" % (cmd, code))
        #os.remove(os.path.join(self.temp_dir, file))

        if self.remove_header:
            cmd = "tail -n +2 %s/%s > %s/tmpfile; mv %s/tmpfile %s/%s" % \
                  (self.temp_dir, self.file, self.temp_dir, self.temp_dir, self.temp_dir, self.file)
            logging.info("Running shell command: %s" % cmd)
            code = subprocess.Popen(cmd, shell=True)
            code.communicate()

        cmd = "sed -e :a -e '/\\\\$/N; s/\\\\\\n//; ta' %s/%s | tr -d '\015' > %s/clean_%s" % (
            self.temp_dir, self.file, self.temp_dir, self.file)
        logging.info("Running shell command: %s" % cmd)
        code = subprocess.Popen(cmd, shell=True)
        code.communicate()
        os.remove(os.path.join(self.temp_dir, self.file))

        # Split the cleaned file
        cmd = [
            "split",
            "-l",
            "200000",
            "%s/clean_%s" % (self.temp_dir, self.file),
            "%s/%s.part." % (self.temp_dir, self.suite)
        ]

        logging.info("Running shell command: %s" % cmd)
        code = subprocess.check_call(cmd)
        logging.info("shell command %s returned code %s" % (cmd, code))
        os.remove(os.path.join(self.temp_dir, "clean_%s" % self.file))

        # Gzip the new part files
        cmd = 'gzip %s/%s.part*' % (self.temp_dir, self.suite)
        logging.info("Running shell command: %s" % cmd)
        code = subprocess.Popen(cmd, shell=True)
        code.communicate()
        logging.info("shell command %s returned code %s" % (cmd, code))

        return code == 0
