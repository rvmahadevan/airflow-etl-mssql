import datetime as dt

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.mssql_operator import MsSqlOperator
import sqlalchemy;
from sqlalchemy import create_engine;
from sqlalchemy.orm import sessionmaker;
import pymssql
from datetime import timedelta

yesterday = dt.date.today() - timedelta(1)
year = int(yesterday.strftime('%Y'))
month = int(yesterday.strftime("%m"))
day = int(yesterday.strftime("%d"))


def read_sql_table():

    print("------------------ READING FROM SOURCE PC_SEAT -------------------------")

    # engine = sqlalchemy.create_engine('mssql+pymssql://testapi:testapi*@13@10.168.195.200:8189/Edgestage');
    engine = sqlalchemy.create_engine('mssql+pymssql://DW_APP_RO:Bip1P@$$w0rd@10.168.193.241:8189/Edge');
    conn = engine.connect();
    Session = sessionmaker();
    session = Session(bind=conn);
    data = session.execute("""SELECT [SeatID],[UserID],[Duration],[EndDate],[StartDate],[Frequency],[FrequencyUnit],
                         [Type],'' [CoachType],[TotalSessions] TotalSessions,'' [Check_Sum],'' [CDCType],[ModifiedDate],[SeatLotID]
                         FROM [users].[dbo].[PC_Seat]
                         WHERE Userid > 0 AND ModifiedDate >  DATEADD(mi, -1500, Current_TimeStamp)""").fetchall();
    print("----------------------------------------------------- CONNECTION ESTABLISHSED AND DATA RETRIVED - SUCCESSS ----------------");

     ### WRITE TO TARGET pc_seat ###
    engine = sqlalchemy.create_engine('mssql+pymssql://ge_testuser:test@123@10.168.195.200:8189/TestDB');
    conn = engine.connect();
    Session = sessionmaker();
    session = Session(bind=conn);
    session.execute("TRUNCATE TABLE TestDB.dbo.PC_Seat_stage");
    print("----------------------------------------------------- Truncated TestDB.dbo.PC_Seat_stage  RETRIVED - SUCCESSS ----------------");
    for row in data:
        rowData = {"SeatID":row[0],"UserID":row[1],"Duration":row[2],"EndDate":row[3],"StartDate":row[4],"Frequency":row[5],"FrequencyUnit":row[6],"Type":row[7]
        ,"CoachType":row[8],"TotalSession":row[9],"Check_Sum":row[10],"CDCType":row[11],"ModifiedDate":row[12],"SeatLotID":row[13]}
        result  = session.execute("""INSERT INTO TestDB.dbo.PC_Seat_Stage
        (SeatID,UserID,Duration,EndDate,StartDate,Frequency,FrequencyUnit,Type
        ,CoachType,TotalSession,Check_Sum,CDCType,ModifiedDate,SeatLotID)
        VALUES (:SeatID,:UserID,:Duration,:EndDate,:StartDate,:Frequency,:FrequencyUnit,
        :Type,:CoachType,:TotalSession,:Check_Sum,:CDCType,:ModifiedDate,:SeatLotID)""",rowData);
        print(result);

    SQL = """MERGE testdb.dbo.pc_seat_real T
             USING testdb.dbo.pc_seat_stage S ON T.SeatID = S.SeatID 
             WHEN MATCHED
             THEN UPDATE SET 
             UserID = S.UserID, T.Duration= S.Duration, EndDate  = S.EndDate, StartDate = S.StartDate, Frequency = S.Frequency,
             FrequencyUnit  = S.FrequencyUnit, CoachType = S.CoachType, TotalSession = S.TotalSession,  Check_Sum = S.Check_Sum,
             CDCType = S.CDCType, ModifiedDate=S.ModifiedDate, SeatLotID = S.SeatLotID
             WHEN NOT MATCHED BY TARGET THEN
             INSERT(SeatID,UserID,Duration,EndDate,StartDate,Frequency,FrequencyUnit,Type
             ,CoachType,TotalSession,Check_Sum,CDCType,ModifiedDate,SeatLotID)
             VALUES(SeatID, UserID,Duration,EndDate,StartDate,Frequency,FrequencyUnit,Type
             ,CoachType,TotalSession,Check_Sum,CDCType,ModifiedDate,SeatLotID)
             WHEN NOT MATCHED BY SOURCE THEN DELETE;""";
    result = session.execute(SQL);
    session.commit();
    conn.close();

#def write_to_data_database_


default_args = {
    'owner': 'me',
    'start_date': dt.datetime(year, month, day),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
}


with DAG('pc_seat',
         default_args=default_args,
         schedule_interval='*/5 * * * *',
         ) as dag:

    merge_pc_seat = PythonOperator(task_id='pc_seat_real',
                                 python_callable=read_sql_table)
    


merge_pc_seat
