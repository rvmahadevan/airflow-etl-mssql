import os, uuid, sys
import pandas as pd
from pandas.io.json import json_normalize
import json
import fileinput
import csv
import ast
from azure.storage.blob import BlockBlobService, PublicAccess, ContentSettings


def create_conn():
    try:
        # Create the BlockBlockService that is used to call the Blob service for the storage account
        block_blob_service = BlockBlobService(account_name='getestdatawarehouse', account_key='ifRsj2CaTO+cjLpWiB9R6ChnwaKI9P9lP9KLXqfKm80xpoCkyglzxRRnz4FeYBCXMvtyDdHBqPef5wFusKnoyg==')
    except Exception as e:
        print(e)

def get_blob_to_text(
        container_name, blob_name, encoding='utf-8', snapshot=None,
        start_range=None, end_range=None, validate_content=False,
        progress_callback=None, max_connections=2, lease_id=None,
        if_modified_since=None, if_unmodified_since=None, if_match=None,
        if_none_match=None, timeout=None):

    block_blob_service = BlockBlobService(account_name='getestdatawarehouse',
                                          account_key='ifRsj2CaTO+cjLpWiB9R6ChnwaKI9P9lP9KLXqfKm80xpoCkyglzxRRnz4FeYBCXMvtyDdHBqPef5wFusKnoyg==')

    '''
    Downloads a blob as unicode text, with automatic chunking and progress
    notifications. Returns an instance of :class:`~azure.storage.blob.models.Blob` with
    properties, metadata, and content.

    :param str container_name:
        Name of existing container.
    :param str blob_name:
        Name of existing blob.
    :param str encoding:
        Python encoding to use when decoding the blob data.
    :param str snapshot:
        The snapshot parameter is an opaque DateTime value that,
        when present, specifies the blob snapshot to retrieve.
    :param int start_range:
        Start of byte range to use for downloading a section of the blob.
        If no end_range is given, all bytes after the start_range will be downloaded.
        The start_range and end_range params are inclusive.
        Ex: start_range=0, end_range=511 will download first 512 bytes of blob.
    :param int end_range:
        End of byte range to use for downloading a section of the blob.
        If end_range is given, start_range must be provided.
        The start_range and end_range params are inclusive.
        Ex: start_range=0, end_range=511 will download first 512 bytes of blob.
    :param bool validate_content:
        If set to true, validates an MD5 hash for each retrieved portion of
        the blob. This is primarily valuable for detecting bitflips on the wire
        if using http instead of https as https (the default) will already
        validate. Note that the service will only return transactional MD5s
        for chunks 4MB or less so the first get request will be of size
        self.MAX_CHUNK_GET_SIZE instead of self.MAX_SINGLE_GET_SIZE. If
        self.MAX_CHUNK_GET_SIZE was set to greater than 4MB an error will be
        thrown. As computing the MD5 takes processing time and more requests
        will need to be done due to the reduced chunk size there may be some
        increase in latency.
    :param progress_callback:
        Callback for progress with signature function(current, total)
        where current is the number of bytes transfered so far, and total is
        the size of the blob if known.
    :type progress_callback: func(current, total)
    :param int max_connections:
        If set to 2 or greater, an initial get will be done for the first
        self.MAX_SINGLE_GET_SIZE bytes of the blob. If this is the entire blob,
        the method returns at this point. If it is not, it will download the
        remaining data parallel using the number of threads equal to
        max_connections. Each chunk will be of size self.MAX_CHUNK_GET_SIZE.
        If set to 1, a single large get request will be done. This is not
        generally recommended but available if very few threads should be
        used, network requests are very expensive, or a non-seekable stream
        prevents parallel download. This may also be useful if many blobs are
        expected to be empty as an extra request is required for empty blobs
        if max_connections is greater than 1.
    :param str lease_id:
        Required if the blob has an active lease.
    :param datetime if_modified_since:
        A DateTime value. Azure expects the date value passed in to be UTC.
        If timezone is included, any non-UTC datetimes will be converted to UTC.
        If a date is passed in without timezone info, it is assumed to be UTC.
        Specify this header to perform the operation only
        if the resource has been modified since the specified time.
    :param datetime if_unmodified_since:
        A DateTime value. Azure expects the date value passed in to be UTC.
        If timezone is included, any non-UTC datetimes will be converted to UTC.
        If a date is passed in without timezone info, it is assumed to be UTC.
        Specify this header to perform the operation only if
        the resource has not been modified since the specified date/time.
    :param str if_match:
        An ETag value, or the wildcard character (*). Specify this header to perform
        the operation only if the resource's ETag matches the value specified.
    :param str if_none_match:
        An ETag value, or the wildcard character (*). Specify this header
        to perform the operation only if the resource's ETag does not match
        the value specified. Specify the wildcard character (*) to perform
        the operation only if the resource does not exist, and fail the
        operation if it does exist.
    :param int timeout:
        The timeout parameter is expressed in seconds. This method may make
        multiple calls to the Azure service and the timeout will apply to
        each call individually.
    :return: A Blob with properties and metadata. If max_connections is greater
        than 1, the content_md5 (if set on the blob) will not be returned. If you
        require this value, either use get_blob_properties or set max_connections
        to 1.
    :rtype: :class:`~azure.storage.blob.models.Blob`
    '''
    blob = block_blob_service.get_blob_to_bytes(container_name,
                                  blob_name)
    blob_content = blob.content.decode(encoding)
    data = map(lambda x: x.rstrip(), blob_content)
    data_json_str = "[" + "".join(data) + "]"
    data_json = data_json_str.replace('}{"_id"', '},{"_id"')
    return data_json


def blob_read_upload(src_container, src_blob, dest_container=None, dest_blob=None):
    data_json_str = get_blob_to_text(src_container, src_blob)
    obj = json.loads(data_json_str)
    for row in obj:
        df = json_normalize(row)
        obj = (df['_id'].map(str) + '|' + df['preference.MONDAY.preferred'].map(str) + '|' + df[
            'preference.TUESDAY.preferred'].map(str) + '|' + df['preference.WEDNESDAY.preferred'].map(str) + '|' + + df[
            'preference.THURSDAY.preferred'].map(str) + '|' + df['preference.FRIDAY.preferred'].map(str) + '|' + df[
            'preference.SATURDAY.preferred'].map(str) + '|' + df['preference.SUNDAY.preferred'].map(str))
        df = obj.replace('"', '')
        df.to_csv(r'learners.txt', header=None, index=None, sep='|', mode='a')

def blob_json_upload(src_container, src_blob, dest_container=None, dest_blob=None):
    data_json_str = get_blob_to_text(src_container, src_blob)
    obj = json.loads(data_json_str)
    for row in obj:
        df = pd.io.json.json_normalize(row)
        df.columns = df.columns.map(lambda x: x.split(".")[-1])
        df.to_csv(r'learners_new.csv', header=True, index=None, sep='|', mode='a')

def blob_json_text(src_container, src_blob):
    block_blob_service = BlockBlobService(account_name='getestdatawarehouse',
                                          account_key='ifRsj2CaTO+cjLpWiB9R6ChnwaKI9P9lP9KLXqfKm80xpoCkyglzxRRnz4FeYBCXMvtyDdHBqPef5wFusKnoyg==')
    data_json_str = get_blob_to_text(src_container, src_blob)
    obj = json.loads(data_json_str)
    # id, monday, tuesday, wednesday, thursday, friday, saturday, sunday = [], [], [], [], [], [], [], []
    for row in obj:
        df = json_normalize(row)
        obj = df['_id'].map(str) + '|' + df['preference.MONDAY.preferred'].map(str) + '|' + df[
            'preference.TUESDAY.preferred'].map(str) + '|' + df['preference.WEDNESDAY.preferred'].map(str) + '|' + + df[
            'preference.THURSDAY.preferred'].map(str) + '|' + df['preference.FRIDAY.preferred'].map(str) + '|' + df[
                   'preference.SATURDAY.preferred'].map(str) + '|' + df['preference.SUNDAY.preferred'].map(str)
        obj.to_csv(r'/Users/mahadevanvaradhan/Documents/MyGitLab/airflow-etl-mssql/src/data/learners.csv', header=None, index=None, mode='a')

def blob_json_load(dest_container, dest_blob):
    block_blob_service = BlockBlobService(account_name='getestdatawarehouse',
                                          account_key='ifRsj2CaTO+cjLpWiB9R6ChnwaKI9P9lP9KLXqfKm80xpoCkyglzxRRnz4FeYBCXMvtyDdHBqPef5wFusKnoyg==')
    block_blob_service.create_blob_from_path(dest_container, dest_blob, 'learners.csv', content_settings=ContentSettings(content_type='application/CSV'))

blob_json_text('coachraw', 'learner_preference\LearnersPreference.json')
# blob_json_load('polybase', 'learner_preference\learners_preference.csv')
