import sys, uuid
from os import path

import yaml
from yaml import load
import urllib

import datetime as dt
from datetime import timedelta, datetime

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
import hashlib
import pandas as pd
from pandas.io.json import json_normalize
import json
import requests
import fileinput
import csv
import ast
from azure.storage.blob import BlockBlobService, PublicAccess, ContentSettings

yesterday = dt.date.today() - timedelta(1)
year = int(yesterday.strftime('%Y'))
month = int(yesterday.strftime("%m"))
day = int(yesterday.strftime("%d"))



yesterday = dt.date.today() - timedelta(1)
year = int(yesterday.strftime('%Y'))
month = int(yesterday.strftime("%m"))
day = int(yesterday.strftime("%d"))

with open('coach_dev.yml', 'r') as f:
    conf = yaml.load(f)

account_name = conf['AzureBlobDev']['account']
account_key = conf['AzureBlobDev']['key']
src_container = conf['CoachContainer']['source']
src_blob = conf['CoachContainer']['learner_preference_src']
dest_container = conf['CoachContainer']['target']
dest_blob = conf['CoachContainer']['learner_preference_tgt']
localfilepath = conf['LocalDir']['session_path']
session_url = conf['CoachAPI']['session']
secretkey = conf['CoachAPI']['secret']
cmd = "print('Hello')"


def sha1(secret):
    utc_date = datetime.utcnow().strftime("%Y%m%d")
    data_to_be_hashed = (secret + utc_date).encode()
    hashed = hashlib.sha1(data_to_be_hashed)
    print(hashed.hexdigest())
    return hashed.hexdigest()



def get_session_data():
    try:
        secret_key = sha1(secretkey)
        url = session_url % (secret_key, 20190415, 20190416)
        print(url)
        with urllib.request.urlopen(url) as url:
            response = json.loads(url.read().decode())
        return response
    except Exception as e:
        raise e

# {'SessionId': '5cb4572e8c58310016b8a628', 'Status': 'scheduled', 'StatusDetail': None,
# 'StartOn': '2019-07-10T21:00:00.000Z', 'EndOn': '2019-07-10T22:00:00.000Z',
# 'Duration': None, 'SessionType': None, 'ConnectionType': None, 'Title': '', 'Note': None,
# 'RecoupedSessionId': None, 'LearnerId': '1605130', 'IsPayable': None, 'LicenseID': '1432562',
# 'LicenseStartDate': None, 'LicenseEndDate': None, 'LearnerFullName': None, 'TrainerId': '1605099',
# 'TrainerFullName': None, 'RatingScore': None, 'RatingComment': None, 'CreatedDate': '2019-04-15T10:03:12.083Z',
# 'LastUpdatedDate': '2019-04-15T10:03:12.083Z', 'StatusHistory': []}



def blob_json_text():
    data_json = get_session_data()
    for row in data_json:
        df = json_normalize(row)
        export_csv = df.to_csv(localfilepath, index=None, header=True, mode='a')

def get_data_coaxch():
    df = blob_json_text()
    print(df)
#
# DAG_NAME = "coach_session"
# SCHEDULE_INTERVAL = "0 8 * * *"
#
#
# DEFAULT_ARGUMENTS = {
#     "owner": "airflow",
#     "depends_on_past": False,
#     "start_date": dt.datetime(2019, 4, 15),
#     "schedule_interval": SCHEDULE_INTERVAL,
#     "email": ['airflow@example.com'],
#     "email_on_failure": True,
#     "email_on_retry": True,
#     "retries": 3,
#     "retry_delay": timedelta(minutes=5),
# }
#
# DAG_ID = DAG(DAG_NAME,
#              schedule_interval=SCHEDULE_INTERVAL,
#              default_args=DEFAULT_ARGUMENTS,
#              )
#
#
# read_learner_csv = PythonOperator(
#     task_id='read_learner_csv_blob',
#     python_callable=blob_json_text,
#     dag=DAG_ID)
#
# load_learner_csv = BashOperator(
#     task_id='load_learner_csv_blob',
#     bash_command=cmd,
#     dag=DAG_ID)
#

get_data_coaxch()
