import sys, uuid
sys.path.append("../../")
from os import path

import yaml
from yaml import load
import urllib

import datetime as dt
from datetime import timedelta, datetime

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
import hashlib

import pandas as pd
from pandas.io.json import json_normalize
import json
import requests
import fileinput
import csv
import ast

from azure.storage.blob import BlockBlobService, PublicAccess, ContentSettings


# from operators.CustomUtilsOperator import CustomPythonCallable


yesterday_ds = dt.date.today() - timedelta(8)
yesterday = int(yesterday_ds.strftime('%Y%m%d'))
today_ds = dt.date.today()
today = int(today_ds.strftime('%Y%m%d'))
tomorrow_ds = dt.date.today() + timedelta(1)
tomorrow = int(tomorrow_ds.strftime('%Y%m%d'))

year = int(today_ds.strftime('%Y'))
month = int(today_ds.strftime("%m"))
day = int(today_ds.strftime("%d"))



with open('/usr/local/airflow/dags/coach/conf/coach_test.yml', 'r') as f:
    CONF = yaml.load(f)


account_name = CONF['AzureBlobDev']['account']
account_key = CONF['AzureBlobDev']['key']
src_container = CONF['CoachContainer']['container']
session_dest_blob = CONF['CoachContainer']['coach_session_tgt']
status_dest_blob = CONF['CoachContainer']['coach_status_tgt']
sessionfilepath = CONF['LocalDir']['session_path']
statusfilepath = CONF['LocalDir']['status_path']
session_url = CONF['CoachAPI']['session']
secret = CONF['CoachAPI']['secret']

def sha1(secret):
    utc_date = datetime.utcnow().strftime("%Y%m%d")
    data_to_be_hashed = (secret + utc_date).encode()
    hashed = hashlib.sha1(data_to_be_hashed)
    print(hashed.hexdigest())
    return hashed.hexdigest()


def get_session_data():
    """https://airflow.readthedocs.io/en/stable/code.html#default-variables"""
    try:
        secret_key = sha1(secret)
        url = session_url % (secret_key, today, tomorrow)
        print(url)
        with urllib.request.urlopen(url) as url:
            response = json.loads(url.read().decode())
        return response
    except Exception as e:
        raise e

def blob_coach_session():
    localfilepath = sessionfilepath % today
    detailfilepath = statusfilepath % today
    data_json = get_session_data()
    df = json_normalize(data_json)
    df = df.where((pd.notnull(df)), 'null')
    df_clean = df.drop(['StatusHistory'], axis=1)
    df_clean.to_csv(localfilepath, index=None, sep='|', header=False, mode='w')
    dsh = df['StatusHistory']
    data = pd.DataFrame([])
    for row in dsh:
        if row != []:
            df_sd = json_normalize(row)
            df_sd = df_sd.where((pd.notnull(df_sd)), 'null')
            data = data.append(df_sd, sort=False)
            data_clean = data.drop(['statusDetail'], axis=1)
    data_clean.to_csv(detailfilepath, index=None, sep='|', header=False, mode='w')


def blob_load_session(**context):
    coach_session_dest_blob = session_dest_blob % today
    status_session_dest_blob = status_dest_blob % today
    localfilepath = sessionfilepath % today
    detailfilepath = statusfilepath % today
    block_blob_service = BlockBlobService(account_name, account_key)
    block_blob_service.create_blob_from_path(src_container, coach_session_dest_blob, localfilepath, content_settings=ContentSettings(content_type='application/CSV'))
    block_blob_service.create_blob_from_path(src_container, status_session_dest_blob, detailfilepath, content_settings=ContentSettings(content_type='application/CSV'))



DAG_NAME = CONF['DAG_CONFIG']['dag_name']
# DATE_OBJ = datetime.strptime(CONF['DAG_CONFIG']['start_date'], "%d/%m/%Y %H:%M:%S")
START_DATE = dt.datetime(year, month, day)
SCHEDULE_INTERVAL = CONF['DAG_CONFIG']['schedule_interval']



DEFAULT_ARGUMENTS = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": START_DATE,
    "schedule_interval": SCHEDULE_INTERVAL,
    "email": ['airflow@example.com'],
    "email_on_failure": True,
    "email_on_retry": True,
    "retries": 3,
    "retry_delay": timedelta(minutes=5)
}


DAG_ID = DAG(DAG_NAME,
             schedule_interval=SCHEDULE_INTERVAL,
             default_args=DEFAULT_ARGUMENTS,
             )

read_session_csv = PythonOperator(
    task_id='read_session_csv_blob',
    provide_context=False,
    python_callable=blob_coach_session,
    dag=DAG_ID)


load_session_csv = PythonOperator(
    task_id='load_session_csv_blob',
    provide_context=False,
    python_callable=blob_load_session,
    dag=DAG_ID)


load_session_csv.set_upstream(read_session_csv)
