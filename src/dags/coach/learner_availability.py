import pandas as pd
from pandas.io.json import json_normalize
import json
import requests
import fileinput
import csv
import ast

import hashlib
import sys, uuid
# sys.path.append("../../..")
from os import path

import yaml
from yaml import load
import urllib

import datetime as dt
import time
from datetime import timedelta, datetime
from azure.storage.blob import BlockBlobService, PublicAccess, ContentSettings
import tables

#import sql libraries
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.mssql_operator import MsSqlOperator
import sqlalchemy;
from sqlalchemy import create_engine;
from sqlalchemy.orm import sessionmaker;
import pymssql


yesterday = dt.date.today() - timedelta(2)
year = int(yesterday.strftime('%Y'))
month = int(yesterday.strftime("%m"))
day = int(yesterday.strftime("%d"))


with open('/usr/local/airflow/dags/coach/conf/coach_test.yml', 'r') as f:
    CONF = yaml.load(f)

account_name = CONF['LEARNER']['STORAGEACCOUNTNAME']
account_key = CONF['LEARNER']['STORAGEACCOUNTKEY']
src_container = CONF['LEARNER']['CONTAINERNAME']
src_blob = CONF['LEARNER']['LEARNERBLOBNAME']
learner_local_input = CONF['LEARNER']['LEARNERLOCAL']
learner_local_output = CONF['LEARNER']['LEARNERLOCALPATH']
learner_destination_blob = CONF['LEARNER']['LEARNERDESTINATIONBLOB']
local_path = CONF['LEARNER']['LOCALPATH']

#download from blob
def read_blob():
    t1=time.time()
    blob_service=BlockBlobService(account_name=account_name,account_key=account_key)
    blob_service.get_blob_to_path(src_container,src_blob,learner_local_input)
    t2=time.time()
    print(("It takes %s seconds to download "+ src_blob) % (t2 - t1))
    df_learner = pd.read_csv(learner_local_input, sep=',')
    return df_learner

def clean_text():
    df_learner = read_blob()
    ##Rename column name
    df_learner.columns = ['userid','monday_s1' ,'monday_s2' ,'monday_s3' ,'monday_s4' ,'monday_s5' ,'monday_s6' ,'monday_s7' ,'monday_s8' ,'monday_s9' ,'monday_s10' ,'monday_s11' ,'monday_s12' ,'monday_s13' ,'monday_s14' ,'monday_s15' ,'monday_s16' ,'monday_s17' ,'monday_s18' ,'monday_s19' ,'monday_s20' ,'monday_s21' ,'monday_s22' ,'monday_s23' ,'monday_s24' ,'monday_s25' ,'monday_s26' ,'monday_s27' ,'monday_s28' ,'monday_s29' ,'monday_s30' ,'monday_s31' ,'monday_s32' ,'monday_s33' ,'monday_s34' ,'monday_s35' ,'monday_s36' ,'monday_s37' ,'monday_s38' ,'monday_s39' ,'monday_s40' ,'monday_s41' ,'monday_s42' ,'monday_s43' ,'monday_s44' ,'monday_s45' ,'monday_s46' ,'monday_s47' ,'monday_s48' ,'monday_s49' ,'monday_s50' ,'tuesday_s1' ,'tuesday_s2' ,'tuesday_s3' ,'tuesday_s4' ,'tuesday_s5' ,'tuesday_s6' ,'tuesday_s7' ,'tuesday_s8' ,'tuesday_s9' ,'tuesday_s10' ,'tuesday_s11' ,'tuesday_s12' ,'tuesday_s13' ,'tuesday_s14' ,'tuesday_s15' ,'tuesday_s16' ,'tuesday_s17' ,'tuesday_s18' ,'tuesday_s19' ,'tuesday_s20' ,'tuesday_s21' ,'tuesday_s22' ,'tuesday_s23' ,'tuesday_s24' ,'tuesday_s25' ,'tuesday_s26' ,'tuesday_s27' ,'tuesday_s28' ,'tuesday_s29' ,'tuesday_s30' ,'tuesday_s31' ,'tuesday_s32' ,'tuesday_s33' ,'tuesday_s34' ,'tuesday_s35' ,'tuesday_s36' ,'tuesday_s37' ,'tuesday_s38' ,'tuesday_s39' ,'tuesday_s40' ,'tuesday_s41' ,'tuesday_s42' ,'tuesday_s43' ,'tuesday_s44' ,'tuesday_s45' ,'tuesday_s46' ,'tuesday_s47' ,'tuesday_s48' ,'tuesday_s49' ,'tuesday_s50' ,'wednesday_s1' ,'wednesday_s2' ,'wednesday_s3' ,'wednesday_s4' ,'wednesday_s5' ,'wednesday_s6' ,'wednesday_s7' ,'wednesday_s8' ,'wednesday_s9' ,'wednesday_s10' ,'wednesday_s11' ,'wednesday_s12' ,'wednesday_s13' ,'wednesday_s14' ,'wednesday_s15' ,'wednesday_s16' ,'wednesday_s17' ,'wednesday_s18' ,'wednesday_s19' ,'wednesday_s20' ,'wednesday_s21' ,'wednesday_s22' ,'wednesday_s23' ,'wednesday_s24' ,'wednesday_s25' ,'wednesday_s26' ,'wednesday_s27' ,'wednesday_s28' ,'wednesday_s29' ,'wednesday_s30' ,'wednesday_s31' ,'wednesday_s32' ,'wednesday_s33' ,'wednesday_s34' ,'wednesday_s35' ,'wednesday_s36' ,'wednesday_s37' ,'wednesday_s38' ,'wednesday_s39' ,'wednesday_s40' ,'wednesday_s41' ,'wednesday_s42' ,'wednesday_s43' ,'wednesday_s44' ,'wednesday_s45' ,'wednesday_s46' ,'wednesday_s47' ,'wednesday_s48' ,'wednesday_s49' ,'wednesday_s50' ,'thursday_s1' ,'thursday_s2' ,'thursday_s3' ,'thursday_s4' ,'thursday_s5' ,'thursday_s6' ,'thursday_s7' ,'thursday_s8' ,'thursday_s9' ,'thursday_s10' ,'thursday_s11' ,'thursday_s12' ,'thursday_s13' ,'thursday_s14' ,'thursday_s15' ,'thursday_s16' ,'thursday_s17' ,'thursday_s18' ,'thursday_s19' ,'thursday_s20' ,'thursday_s21' ,'thursday_s22' ,'thursday_s23' ,'thursday_s24' ,'thursday_s25' ,'thursday_s26' ,'thursday_s27' ,'thursday_s28' ,'thursday_s29' ,'thursday_s30' ,'thursday_s31' ,'thursday_s32' ,'thursday_s33' ,'thursday_s34' ,'thursday_s35' ,'thursday_s36' ,'thursday_s37' ,'thursday_s38' ,'thursday_s39' ,'thursday_s40' ,'thursday_s41' ,'thursday_s42' ,'thursday_s43' ,'thursday_s44' ,'thursday_s45' ,'thursday_s46' ,'thursday_s47' ,'thursday_s48' ,'thursday_s49' ,'thursday_s50' ,'friday_s1' ,'friday_s2' ,'friday_s3' ,'friday_s4' ,'friday_s5' ,'friday_s6' ,'friday_s7' ,'friday_s8' ,'friday_s9' ,'friday_s10' ,'friday_s11' ,'friday_s12' ,'friday_s13' ,'friday_s14' ,'friday_s15' ,'friday_s16' ,'friday_s17' ,'friday_s18' ,'friday_s19' ,'friday_s20' ,'friday_s21' ,'friday_s22' ,'friday_s23' ,'friday_s24' ,'friday_s25' ,'friday_s26' ,'friday_s27' ,'friday_s28' ,'friday_s29' ,'friday_s30' ,'friday_s31' ,'friday_s32' ,'friday_s33' ,'friday_s34' ,'friday_s35' ,'friday_s36' ,'friday_s37' ,'friday_s38' ,'friday_s39' ,'friday_s40' ,'friday_s41' ,'friday_s42' ,'friday_s43' ,'friday_s44' ,'friday_s45' ,'friday_s46' ,'friday_s47' ,'friday_s48' ,'friday_s49' ,'friday_s50' ,'saturday_s1' ,'saturday_s2' ,'saturday_s3' ,'saturday_s4' ,'saturday_s5' ,'saturday_s6' ,'saturday_s7' ,'saturday_s8' ,'saturday_s9' ,'saturday_s10' ,'saturday_s11' ,'saturday_s12' ,'saturday_s13' ,'saturday_s14' ,'saturday_s15' ,'saturday_s16' ,'saturday_s17' ,'saturday_s18' ,'saturday_s19' ,'saturday_s20' ,'saturday_s21' ,'saturday_s22' ,'saturday_s23' ,'saturday_s24' ,'saturday_s25' ,'saturday_s26' ,'saturday_s27' ,'saturday_s28' ,'saturday_s29' ,'saturday_s30' ,'saturday_s31' ,'saturday_s32' ,'saturday_s33' ,'saturday_s34' ,'saturday_s35' ,'saturday_s36' ,'saturday_s37' ,'saturday_s38' ,'saturday_s39' ,'saturday_s40' ,'saturday_s41' ,'saturday_s42' ,'saturday_s43' ,'saturday_s44' ,'saturday_s45' ,'saturday_s46' ,'saturday_s47' ,'saturday_s48' ,'saturday_s49' ,'saturday_s50' ,'sunday_s1' ,'sunday_s2' ,'sunday_s3' ,'sunday_s4' ,'sunday_s5' ,'sunday_s6' ,'sunday_s7' ,'sunday_s8' ,'sunday_s9' ,'sunday_s10' ,'sunday_s11' ,'sunday_s12' ,'sunday_s13' ,'sunday_s14' ,'sunday_s15' ,'sunday_s16' ,'sunday_s17' ,'sunday_s18' ,'sunday_s19' ,'sunday_s20' ,'sunday_s21' ,'sunday_s22' ,'sunday_s23' ,'sunday_s24' ,'sunday_s25' ,'sunday_s26' ,'sunday_s27' ,'sunday_s28' ,'sunday_s29' ,'sunday_s30' ,'sunday_s31' ,'sunday_s32' ,'sunday_s33' ,'sunday_s34' ,'sunday_s35' ,'sunday_s36' ,'sunday_s37' ,'sunday_s38' ,'sunday_s39' ,'sunday_s40' ,'sunday_s41' ,'sunday_s42' ,'sunday_s43' ,'sunday_s44' ,'sunday_s45' ,'sunday_s46' ,'sunday_s47' ,'sunday_s48' ,'sunday_s49' ,'sunday_s50']
    df_learner_clean = df_learner.where((pd.notnull(df_learner)), '')
    #Unpivot the learner availability
    df_learner_final = pd.melt(df_learner_clean, id_vars =['userid'], value_vars =['monday_s1' ,'monday_s2' ,'monday_s3' ,'monday_s4' ,'monday_s5' ,'monday_s6' ,'monday_s7' ,'monday_s8' ,'monday_s9' ,'monday_s10' ,'monday_s11' ,'monday_s12' ,'monday_s13' ,'monday_s14' ,'monday_s15' ,'monday_s16' ,'monday_s17' ,'monday_s18' ,'monday_s19' ,'monday_s20' ,'monday_s21' ,'monday_s22' ,'monday_s23' ,'monday_s24' ,'monday_s25' ,'monday_s26' ,'monday_s27' ,'monday_s28' ,'monday_s29' ,'monday_s30' ,'monday_s31' ,'monday_s32' ,'monday_s33' ,'monday_s34' ,'monday_s35' ,'monday_s36' ,'monday_s37' ,'monday_s38' ,'monday_s39' ,'monday_s40' ,'monday_s41' ,'monday_s42' ,'monday_s43' ,'monday_s44' ,'monday_s45' ,'monday_s46' ,'monday_s47' ,'monday_s48' ,'monday_s49' ,'monday_s50' ,'tuesday_s1' ,'tuesday_s2' ,'tuesday_s3' ,'tuesday_s4' ,'tuesday_s5' ,'tuesday_s6' ,'tuesday_s7' ,'tuesday_s8' ,'tuesday_s9' ,'tuesday_s10' ,'tuesday_s11' ,'tuesday_s12' ,'tuesday_s13' ,'tuesday_s14' ,'tuesday_s15' ,'tuesday_s16' ,'tuesday_s17' ,'tuesday_s18' ,'tuesday_s19' ,'tuesday_s20' ,'tuesday_s21' ,'tuesday_s22' ,'tuesday_s23' ,'tuesday_s24' ,'tuesday_s25' ,'tuesday_s26' ,'tuesday_s27' ,'tuesday_s28' ,'tuesday_s29' ,'tuesday_s30' ,'tuesday_s31' ,'tuesday_s32' ,'tuesday_s33' ,'tuesday_s34' ,'tuesday_s35' ,'tuesday_s36' ,'tuesday_s37' ,'tuesday_s38' ,'tuesday_s39' ,'tuesday_s40' ,'tuesday_s41' ,'tuesday_s42' ,'tuesday_s43' ,'tuesday_s44' ,'tuesday_s45' ,'tuesday_s46' ,'tuesday_s47' ,'tuesday_s48' ,'tuesday_s49' ,'tuesday_s50' ,'wednesday_s1' ,'wednesday_s2' ,'wednesday_s3' ,'wednesday_s4' ,'wednesday_s5' ,'wednesday_s6' ,'wednesday_s7' ,'wednesday_s8' ,'wednesday_s9' ,'wednesday_s10' ,'wednesday_s11' ,'wednesday_s12' ,'wednesday_s13' ,'wednesday_s14' ,'wednesday_s15' ,'wednesday_s16' ,'wednesday_s17' ,'wednesday_s18' ,'wednesday_s19' ,'wednesday_s20' ,'wednesday_s21' ,'wednesday_s22' ,'wednesday_s23' ,'wednesday_s24' ,'wednesday_s25' ,'wednesday_s26' ,'wednesday_s27' ,'wednesday_s28' ,'wednesday_s29' ,'wednesday_s30' ,'wednesday_s31' ,'wednesday_s32' ,'wednesday_s33' ,'wednesday_s34' ,'wednesday_s35' ,'wednesday_s36' ,'wednesday_s37' ,'wednesday_s38' ,'wednesday_s39' ,'wednesday_s40' ,'wednesday_s41' ,'wednesday_s42' ,'wednesday_s43' ,'wednesday_s44' ,'wednesday_s45' ,'wednesday_s46' ,'wednesday_s47' ,'wednesday_s48' ,'wednesday_s49' ,'wednesday_s50' ,'thursday_s1' ,'thursday_s2' ,'thursday_s3' ,'thursday_s4' ,'thursday_s5' ,'thursday_s6' ,'thursday_s7' ,'thursday_s8' ,'thursday_s9' ,'thursday_s10' ,'thursday_s11' ,'thursday_s12' ,'thursday_s13' ,'thursday_s14' ,'thursday_s15' ,'thursday_s16' ,'thursday_s17' ,'thursday_s18' ,'thursday_s19' ,'thursday_s20' ,'thursday_s21' ,'thursday_s22' ,'thursday_s23' ,'thursday_s24' ,'thursday_s25' ,'thursday_s26' ,'thursday_s27' ,'thursday_s28' ,'thursday_s29' ,'thursday_s30' ,'thursday_s31' ,'thursday_s32' ,'thursday_s33' ,'thursday_s34' ,'thursday_s35' ,'thursday_s36' ,'thursday_s37' ,'thursday_s38' ,'thursday_s39' ,'thursday_s40' ,'thursday_s41' ,'thursday_s42' ,'thursday_s43' ,'thursday_s44' ,'thursday_s45' ,'thursday_s46' ,'thursday_s47' ,'thursday_s48' ,'thursday_s49' ,'thursday_s50' ,'friday_s1' ,'friday_s2' ,'friday_s3' ,'friday_s4' ,'friday_s5' ,'friday_s6' ,'friday_s7' ,'friday_s8' ,'friday_s9' ,'friday_s10' ,'friday_s11' ,'friday_s12' ,'friday_s13' ,'friday_s14' ,'friday_s15' ,'friday_s16' ,'friday_s17' ,'friday_s18' ,'friday_s19' ,'friday_s20' ,'friday_s21' ,'friday_s22' ,'friday_s23' ,'friday_s24' ,'friday_s25' ,'friday_s26' ,'friday_s27' ,'friday_s28' ,'friday_s29' ,'friday_s30' ,'friday_s31' ,'friday_s32' ,'friday_s33' ,'friday_s34' ,'friday_s35' ,'friday_s36' ,'friday_s37' ,'friday_s38' ,'friday_s39' ,'friday_s40' ,'friday_s41' ,'friday_s42' ,'friday_s43' ,'friday_s44' ,'friday_s45' ,'friday_s46' ,'friday_s47' ,'friday_s48' ,'friday_s49' ,'friday_s50' ,'saturday_s1' ,'saturday_s2' ,'saturday_s3' ,'saturday_s4' ,'saturday_s5' ,'saturday_s6' ,'saturday_s7' ,'saturday_s8' ,'saturday_s9' ,'saturday_s10' ,'saturday_s11' ,'saturday_s12' ,'saturday_s13' ,'saturday_s14' ,'saturday_s15' ,'saturday_s16' ,'saturday_s17' ,'saturday_s18' ,'saturday_s19' ,'saturday_s20' ,'saturday_s21' ,'saturday_s22' ,'saturday_s23' ,'saturday_s24' ,'saturday_s25' ,'saturday_s26' ,'saturday_s27' ,'saturday_s28' ,'saturday_s29' ,'saturday_s30' ,'saturday_s31' ,'saturday_s32' ,'saturday_s33' ,'saturday_s34' ,'saturday_s35' ,'saturday_s36' ,'saturday_s37' ,'saturday_s38' ,'saturday_s39' ,'saturday_s40' ,'saturday_s41' ,'saturday_s42' ,'saturday_s43' ,'saturday_s44' ,'saturday_s45' ,'saturday_s46' ,'saturday_s47' ,'saturday_s48' ,'saturday_s49' ,'saturday_s50' ,'sunday_s1' ,'sunday_s2' ,'sunday_s3' ,'sunday_s4' ,'sunday_s5' ,'sunday_s6' ,'sunday_s7' ,'sunday_s8' ,'sunday_s9' ,'sunday_s10' ,'sunday_s11' ,'sunday_s12' ,'sunday_s13' ,'sunday_s14' ,'sunday_s15' ,'sunday_s16' ,'sunday_s17' ,'sunday_s18' ,'sunday_s19' ,'sunday_s20' ,'sunday_s21' ,'sunday_s22' ,'sunday_s23' ,'sunday_s24' ,'sunday_s25' ,'sunday_s26' ,'sunday_s27' ,'sunday_s28' ,'sunday_s29' ,'sunday_s30' ,'sunday_s31' ,'sunday_s32' ,'sunday_s33' ,'sunday_s34' ,'sunday_s35' ,'sunday_s36' ,'sunday_s37' ,'sunday_s38' ,'sunday_s39' ,'sunday_s40' ,'sunday_s41' ,'sunday_s42' ,'sunday_s43' ,'sunday_s44' ,'sunday_s45' ,'sunday_s46' ,'sunday_s47' ,'sunday_s48' ,'sunday_s49' ,'sunday_s50'])
    #Split Start and End time
    df_learner_final[['StartOn','EndOn']] = df_learner_final.value.str.split("-",expand=True,)
    #Split Day and SlotNo
    df_learner_final[['day', 'slot']] = df_learner_final.variable.str.split("_", expand=True)
    #Provide readable column name
    df_learner_final.columns = ['UserId', 'LearnerSlot', 'Scheduled', 'StartOn', 'EndOn', 'Day', 'SlotNo']
    #Drop unnecessaruy column
    learner_final = df_learner_final.drop(['LearnerSlot', 'SlotNo'], axis=1)
    learner_final.to_csv(learner_local_output, index=None, sep='|', header=False, mode='w')
    return learner_final


def blob_load_session():
    block_blob_service = BlockBlobService(account_name, account_key)
    block_blob_service.create_blob_from_path(src_container, learner_destination_blob, learner_local_output, content_settings=ContentSettings(content_type='application/CSV'))


def remove_files():
    cmd = [
        "rm %s/learner_*.csv" % local_path
    ]
    try:
        return subprocess.check_call(cmd, shell=True)
    except ValueError as e:
        print(e);



# def read_sql_table():
#     data = clean_text()
#     print("------------------ READING FROM AVAILABILITY -------------------------")
#     engine = sqlalchemy.create_engine('mssql+pymssql://ge_testuser:test@123@10.168.192.138:8189/Coach');
#     conn = engine.connect();
#     Session = sessionmaker();
#     session = Session(bind=conn);
#     session.execute("TRUNCATE TABLE Coach.stage.LearnerPreference");
#     print("----------------------------------------------------- Truncated Coach.stage.LearnerPreference  RETRIVED - SUCCESSS ----------------");
#     for row in data:
#         print(row)
#         rowData = {"UserId":row[0],"Scheduled":row[1],"StartOn":row[2],"EndOn":row[3],"ScheduledDay":row[4]}
#         result  = session.execute("""INSERT INTO Coach.stage.LearnerPreference
#         (UserId, Scheduled, Starton, Endon, ScheduledDay)
#         VALUES (:UserId, :Scheduled, :StartOn, :EndOn, :ScheduledDay)""", rowData);
#         # print(result);
#
#     # SQL = """MERGE testdb.dbo.pc_seat_real T
#     #          USING testdb.dbo.pc_seat_stage S ON T.SeatID = S.SeatID
#     #          WHEN MATCHED
#     #          THEN UPDATE SET
#     #          UserID = S.UserID, T.Duration= S.Duration, EndDate  = S.EndDate, StartDate = S.StartDate, Frequency = S.Frequency,
#     #          FrequencyUnit  = S.FrequencyUnit, CoachType = S.CoachType, TotalSession = S.TotalSession,  Check_Sum = S.Check_Sum,
#     #          CDCType = S.CDCType, ModifiedDate=S.ModifiedDate, SeatLotID = S.SeatLotID
#     #          WHEN NOT MATCHED BY TARGET THEN
#     #          INSERT(SeatID,UserID,Duration,EndDate,StartDate,Frequency,FrequencyUnit,Type
#     #          ,CoachType,TotalSession,Check_Sum,CDCType,ModifiedDate,SeatLotID)
#     #          VALUES(SeatID, UserID,Duration,EndDate,StartDate,Frequency,FrequencyUnit,Type
#     #          ,CoachType,TotalSession,Check_Sum,CDCType,ModifiedDate,SeatLotID)
#     #          WHEN NOT MATCHED BY SOURCE THEN DELETE;""";
#     # result = session.execute(SQL);
#     session.commit();
#     conn.close();



DEFAULT_ARGUMENTS = {
    "owner": "me",
    "depends_on_past": False,
    "start_date": dt.datetime(year, month, day),
    "email": ['mahadevan.varadhan@globalenglish.com'],
    "email_on_failure": True,
    "email_on_retry": True,
    "retries": 3,
    "retry_delay": timedelta(minutes=5),
}


with DAG('learner_availability',
         default_args=DEFAULT_ARGUMENTS,
         schedule_interval='0 8 * * *',
         ) as dag:

    learner_read_preference = PythonOperator(task_id='load_learner_preference',
                                 python_callable=clean_text)

    learner_load_blob = PythonOperator(task_id='load_learner_blob',
                                             python_callable=blob_load_session)

    remove_temp_files = PythonOperator(task_id='remove_temp_files',
                                       python_callable=remove_files)




learner_read_preference >> learner_load_blob >> remove_temp_files
