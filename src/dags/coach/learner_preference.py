import sys, uuid
from os import path

import yaml
from yaml import load

import datetime as dt
from datetime import timedelta

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

import pandas as pd
from pandas.io.json import json_normalize
import json
import fileinput
import csv
import ast
from azure.storage.blob import BlockBlobService, PublicAccess, ContentSettings


yesterday = dt.date.today() - timedelta(1)
year = int(yesterday.strftime('%Y'))
month = int(yesterday.strftime("%m"))
day = int(yesterday.strftime("%d"))


yesterday = dt.date.today() - timedelta(1)
year = int(yesterday.strftime('%Y'))
month = int(yesterday.strftime("%m"))
day = int(yesterday.strftime("%d"))


with open('/usr/local/airflow/dags/coach/conf/coach_test.yml', 'r') as f:
    conf = yaml.load(f)


account_name = conf['AzureBlobDev']['account']
account_key = conf['AzureBlobDev']['key']
src_container = conf['CoachContainer']['source']
src_blob = conf['CoachContainer']['learner_preference_src']
dest_container = conf['CoachContainer']['target']
dest_blob = conf['CoachContainer']['learner_preference_tgt']
localfilepath = conf['LocalDir']['learner_path']


def get_json_to_text(encoding='utf-8'):

    block_blob_service = BlockBlobService(account_name, account_key)
    blob = block_blob_service.get_blob_to_bytes(src_container, src_blob)
    blob_content = blob.content.decode(encoding)
    data = map(lambda x: x.rstrip(), blob_content)
    data_json_str = "[" + "".join(data) + "]"
    data_json = data_json_str.replace('}{"_id"', '},{"_id"')
    return data_json


def blob_json_text():
    data_json_str = get_json_to_text()
    obj = json.loads(data_json_str)
    # id, monday, tuesday, wednesday, thursday, friday, saturday, sunday = [], [], [], [], [], [], [], []
    for row in obj:
        df = json_normalize(row)
        obj = df['_id'].map(str) + '|' + df['preference.MONDAY.preferred'].map(str) + '|' + df[
            'preference.TUESDAY.preferred'].map(str) + '|' + df['preference.WEDNESDAY.preferred'].map(str) + '|' + + df[
            'preference.THURSDAY.preferred'].map(str) + '|' + df['preference.FRIDAY.preferred'].map(str) + '|' + df[
                   'preference.SATURDAY.preferred'].map(str) + '|' + df['preference.SUNDAY.preferred'].map(str)
        obj.to_csv(localfilepath, header=None, index=None, mode='a')


def blob_json_load():
    block_blob_service = BlockBlobService(account_name, account_key)
    block_blob_service.create_blob_from_path(dest_container, dest_blob, localfilepath, content_settings=ContentSettings(content_type='application/CSV'))


DAG_NAME = "learner_preference"
#SQL_CONN_ID = MSSQL_DEV
SCHEDULE_INTERVAL = "0 8 * * *"


DEFAULT_ARGUMENTS = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": dt.datetime(year, month, day),
    "schedule_interval": SCHEDULE_INTERVAL,
    "email": ['airflow@example.com'],
    "email_on_failure": True,
    "email_on_retry": True,
    "retries": 3,
    "retry_delay": timedelta(minutes=5),
}

DAG_ID = DAG(DAG_NAME,
             schedule_interval=SCHEDULE_INTERVAL,
             default_args=DEFAULT_ARGUMENTS,
             )

read_learner_csv = PythonOperator(
    task_id='read_learner_csv_blob',
    python_callable=blob_json_text,
    dag=DAG_ID)


load_learner_csv = PythonOperator(
    task_id='load_learner_csv_blob',
    python_callable=blob_json_load,
    dag=DAG_ID)


load_learner_csv.set_upstream(read_learner_csv)
