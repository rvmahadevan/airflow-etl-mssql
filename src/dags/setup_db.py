# -*- coding: utf-8 -*-

from __future__ import print_function
import airflow
import logging
from datetime import datetime, timedelta
from airflow import models
from airflow.settings import Session
from airflow.models import Variable
from etl.operators import MsSqlOperator, SqlcmdOperator, SqlcmdFilesOperator


args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(7),
    'provide_context': True
}

tmpl_search_path = Variable.get('sql_path')

dag = airflow.DAG(
    'setup_db',
    schedule_interval="@once",
    default_args=args,
    template_searchpath=Variable.get('sql_path'),
    max_active_runs=1
)


# Create linked server
t2 = SqlcmdFilesOperator(
    task_id='load_into_employee',
    sql='sql/insert_employee.sql',
    mssql_conn_id='MSSQL_DEV16',
    env={
        'sql_file_path': Variable.get('sql_path')
    },
    dag=dag
)


t3 = SqlcmdFilesOperator(
    task_id='update_employee',
    sql='sql/update_employee.sql',
    mssql_conn_id='MSSQL_DEV16',
    env={
        'sql_file_path': Variable.get('sql_path')
    },
    dag=dag
)

t2 >> t3
