"""
Code that goes along with the Airflow tutorial located at:
https://github.com/apache/incubator-airflow/blob/master/airflow/example_dags/tutorial.py
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from etl.operators import MsSqlOperator, SqlcmdOperator, SqlcmdFilesOperator
from airflow.models import Variable


DAG_NAME = "myfirstdagsql"
#SQL_CONN_ID = MSSQL_DEV
SCHEDULE_INTERVAL = "0/5 * * * *"


DEFAULT_ARGUMENTS = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2019, 3, 3),
    "schedule_interval": SCHEDULE_INTERVAL,
    "email": ['airflow@example.com'],
    "email_on_failure": True,
    "email_on_retry": True,
    "retries": 3,
    "retry_delay": timedelta(minutes=5),
}

DAG_ID = DAG(DAG_NAME,
             schedule_interval=SCHEDULE_INTERVAL,
             default_args=DEFAULT_ARGUMENTS,
             template_searchpath=Variable.get('sql_path'),
             )

# t1, t2 and t3 are examples of tasks created by instantiating operators
t1 = BashOperator(
    task_id='print_date',
    bash_command='date',
    dag=DAG_ID)

t2 = MsSqlOperator(
    task_id="sql_load_into_employee",
    mssql_conn_id='MSSQL_DEV',
    sql="Insert into TestDB.dbo.employee(firstname, email, status, modified_date) values ('maha','maha@g.com', 'active', GETDATE())",
    env={},
    dag=DAG_ID)

t3 = MsSqlOperator(
    task_id="sql_update_employeelog",
    mssql_conn_id='MSSQL_DEV',
    sql="update_employee.sql",
    env={},
    dag=DAG_ID)



t2.set_upstream(t1)
t3.set_upstream(t2)


# create table dbo.myuser(userid int not null, username varchar(100) not null)
