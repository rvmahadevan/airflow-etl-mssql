
MERGE TestDB.dbo.employeelog D
USING TestDB.dbo.employee S ON S.UserId = D.UserId
WHEN MATCHED THEN UPDATE
SET userstatus = S.STATUS
WHEN NOT MATCHED BY TARGET THEN  
INSERT (userid, firstname, email, userstatus, modified_date, inserted_date) 
VALUES (s.userid, s.firstname, s.email, s.status, getdate(), getdate());
