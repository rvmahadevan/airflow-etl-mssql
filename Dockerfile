FROM puckel/docker-airflow:1.10.0-2

USER root

RUN apt-get update && apt-get install -y \
    gnupg2 \
    curl apt-transport-https debconf-utils \
    && rm -rf /var/lib/apt/lists/*

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    curl https://packages.microsoft.com/config/debian/8/prod.list > /etc/apt/sources.list.d/mssql-release.list

# Install SQL Server drivers and client tools.

ENV ACCEPT_EULA=Y

RUN apt-get update && \
    apt-get upgrade -y libc6

RUN apt-get install -y msodbcsql mssql-tools unixodbc-dev && \
    apt-get install -y --reinstall --upgrade g++ gcc

# Install python dependencies.
RUN /bin/bash -c "source ~/.bashrc" && \
    pip install --upgrade six && \
    pip install pyodbc psycopg2-binary && \
    pip install azure-storage-blob && \
    pip install azure-storage-file && \
    pip install azure-storage-queue && \
    pip install tables


COPY requirements.txt /requirements.txt
RUN pip install --upgrade --no-cache-dir -r /requirements.txt

COPY /src/msconfig/ /etc/
COPY /src/msconfig/ /usr/local/etc/

RUN echo "deb http://deb.debian.org/debian jessie main" >> /etc/apt/sources.list
RUN apt-get update && \
    apt-get install libssl1.0.0


USER airflow

ARG AIRFLOW_VERSION=1.10.1
ARG AIRFLOW_HOME=/usr/local/airflow
ARG sql_path=/usr/local/airflow/sql
ENV AIRFLOW_GPL_UNIDECODE yes
ARG AIRFLOW_DEPS=""
ARG PYTHON_DEPS=""
ENV AIRFLOW_GPL_UNIDECODE yes

RUN pip install fabric3 flask_bcrypt slackclient boto3 --user

RUN mkdir -p ${AIRFLOW_HOME}/config
RUN mkdir -p ${AIRFLOW_HOME}/dags
RUN mkdir -p ${AIRFLOW_HOME}/operators
RUN mkdir -p ${AIRFLOW_HOME}/etl
RUN mkdir -p ${AIRFLOW_HOME}/common
RUN mkdir -p ${AIRFLOW_HOME}/data
RUN mkdir -p ${AIRFLOW_HOME}/scripts
RUN mkdir -p ${AIRFLOW_HOME}/sql

COPY /src/config ${AIRFLOW_HOME}/config
COPY /src/dags ${AIRFLOW_HOME}/dags
COPY /src/operators ${AIRFLOW_HOME}/operators
COPY /src/etl ${AIRFLOW_HOME}/etl
COPY /src/sql ${AIRFLOW_HOME}/sql
COPY /src/scripts ${AIRFLOW_HOME}/scripts
COPY /src/data ${AIRFLOW_HOME}/data
COPY /src/common ${AIRFLOW_HOME}/common
COPY /src/config/airflow.cfg ${AIRFLOW_HOME}/airflow.cfg


ENV PATH="$PATH:/opt/mssql-tools/bin"


